# Spirotroller Gaming
This Spirotroller version is an evolution of the Spirotroller to provide an improved development platform for basic breath driven gaming devices.

![CAD Image Spirotroller](Docs/Spirotroller_Gaming_CAD.PNG)

The key areas of improvement are in:
### Bluetooth Low Energy and Unity Integration
- Emulate BLE Gamepad
- Communicate with games through Unity Input Manager/System
- Cross platform support including Web based games


### More options for measurements
- Dual pressure sensors for flow rate, direction, and pressure
- Optional restrictor add-on to create back pressure

This version draws from the developments of both the existing Spirotroller versions and from the Open Gamepad.

### Evolutionary Approach
This version is intended to be a development platform for the following activities:
- Update existing and create new Unity based games
- Data visualisation in Unity for breath data
- New Therapies and measurements
- Medical device compliance and sterilisation
- Improved packaging for aesthetic and usage
- Gaming performance evaluation
- Hardware improvements, cost reduction and easy assembly

## Resources

### [Operation Guide](Docs/OpGuide.md)

### [Firmware and Software Development](Docs/Firmware.md)

### [CAD design](Docs/CAD.md)

### [Battery Installation Guide](Docs/Battery_Guide.md)

## Contributors
Contributors are welcome. To join, contact info (at) breathinggames.net

- Jim Anastassio
- Matthew Brown
- Tiberius Brastaviceanu
- Richard Ibbotson
- Povilas Jurgaitis
- Emmanuel Kellner
- Humberto Quintana
- Andres Romero Vasquez
- Kevin Yang



