# Firmware and Software Development 

Both the Open Gamepad and the Spirotroller Gaming are based around the ESP32 processor and the preferred software development environment is Arduino. The devices connect using Bluetooth Low Energy(BLE) to applications running on a variety of gaming and processors. The preferred application development environment is Unity.

## ESP32 Arduino Development

1. Install the latest Arduino IDE from here: https://www.arduino.cc/en/software

2. Add the ESP32 board support from here: https://github.com/espressif/arduino-esp32/blob/master/docs/arduino-ide/boards_manager.md

3. Both the Open Gamepad and the Spirotroller Gaming use the ESP32-BLE-Gamepad library from lemmingDev. It can be loaded using the Arduino Library manager: https://github.com/lemmingDev/ESP32-BLE-Gamepad

4. Both the Open Gamepad and the Spirotroller Gaming use the Adafruit_LPS35HW library for the pressure sensors: It can be loaded using the Arduino Library manager: https://github.com/adafruit/Adafruit_LPS35HW

5. The Open Gamepad uses the Adafruit-MCP23017-Arduino-Library.It can be loaded using the Arduino Library manager:

6. Example code for the Open Gamepad is available on the Breathinggames Gitlab Here: [Open Gamepad](https://gitlab.com/breathinggames/bg_device/-/tree/master/k_open_gamepad/firmware/Open_Gamepad_LPS33)

7. Example code for the Spirotroller Gaming is available on the Breathinggames Gitlab Here: [Spirotroller](https://gitlab.com/breathinggames/bg_device/-/tree/master/j_spirotroller_gaming/Firmware/Spirotroller_Gaming_Zero)


8. The Open Gamepad or the Spirotroller Gaming are connected to the Arduino envionment via the USB cable for firmware download. the devivce will show in the IDE as a serial device, select this COM port and the board should be selected as 'ESP32 Dev Module'. Firmware can then be uploaded.

## Bluetooth BLE connection

### Windows 10

To connect and test the BLE connection the following steps are suggested:

1. Open Window settings, 'Devices'.
2. Click on 'Add Bluetooth or other device'
3. Ensure device is powered on
4. Click 'bluetooth in 'add a device'
5. The Open Gamepad or Spirotroller should appear as a device
6. Click the device and wait for the drivers to install and for the device to ready.
7. Open Windows 'Devices and Printers'
8. The device should now be visible here.
9. Left click on the device and select 'Game controller settings'
10. The controller should appear in the window as an '8 axis 16 button device with hat switch'
11. Select this device and open 'Properties'
12. This window should display the state of the axes and buttons.
13. Open the website https://gamepad-tester.com/ in your browser.
14. The gamepad should appear as 'e502-bbab-Unknown Gamepad' and the axes and button state displayed.




#### Note: 
- Both the Open Gamepad and the Spirotroller Gaming will revert to a low power sleep state if no buttons are pressed for 10 minutes. The Open Gamepad can be awoken by pressing the 'start' button and the Spirotroller awoken by pressing button 1.

- The Spirotroller has an RGB LED which will show green when the device is awake and not connected to Bluetooth. When connected to Bluetooth the LED will be Blue. When the Sprirotroller is in volume accumulate mode the red LED will light in addition


## Unity Interface

The Device should now be visible with the Unity Input Manager and Unity Input system. To test this:



