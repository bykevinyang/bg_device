# Battery Installation Guide for Open Gamepad and Spirotroller Gaming

Due to problems shipping breathinggames devices by air with lithium batteries installed this guide is to assist in the local sourcing and fitting of suitable batteries for the Open Gamepad and Spirotroller Gaming devices.

Both the Open Gamepad and the Spirotroller Gaming use the same ESP32 processor module with integrated Lithium Polymer battery charger for 1S 3.7V battery from the USB power. The battery is connected to the ESP32 module via a 2 pin JST PH 2.0 connector. The 2.0 describes the pin spacing of 2mm. Also of importance is the polarity of the connector with the positive battery pin being at the outer side edge of the module and marked with a '+' sign. 

The specified batteries below will fit and have built in protection against over charge and over discharge. Alternatives may fit but should be of protected type and not capbel of being physically damaged. 

![Lolin32 Lite Module](Lolin32_Lite.jpg)

The ESP32 modules are modified during device build to add a potential divider connected to an ESP32 Analog to Digital converter (ADC) pin to allow the battery voltage to be measured. On the Spirotroller Gaming female headers are also added during build while on the Open Gamepad wiring is direct soldered to the module during build.

![Modified Module](Lolin32_modified.jpg)

## Open Gamepad

The battery used in the Open Gamepad prototype is the type 503040 1S 600mAh and is physically 40mm long, 30mm wide, and 5mm high. This battery fits easily within the space under the keypad after being wrapped in insulation tape to protect it from damage by pins on the underside of the keypad.

![503040 Battery](503040_battery.jpg)

[Link to AliExpress Supplier](https://www.aliexpress.com/item/825607802.html)

![Battery Installed](opengamepad_install.jpg)

The 503040 type batteries are rarely available with a 2 pin JST PH 2.0 connector so this needs to be added as either a crimped plug or an extension cable. Take care to get the polarity correct, reverse polarity will damage the ESP32 module. If you do source the battery with a 2 pin JST PH 2.0 already fitted then do check the polarity (50/50 it is wrong way round for this setup!)


[JST PH 2.0 Datasheet](https://www.jst-mfg.com/product/pdf/eng/ePH.pdf)

[JST connector pin on Digikey](https://www.digikey.co.uk/product-detail/en/jst-sales-america-inc/SPH-002T-P0-5L/455-2148-1-ND/1634657)

[JST 2 pin Housing on Digikey](https://www.digikey.co.uk/product-detail/en/jst-sales-america-inc/PHR-2/455-1165-ND/608607)

[Kit of JST PH 2.0 on Amazon](https://www.amazon.co.uk/dp/B0731MZCGF)

[ Premade cables on Amazon](https://www.amazon.co.uk/Pieces-Micro-Connector-Silicone-Female-Black/dp/B07449V33P)

## Spirotroller Gaming

The battery used in the Open Gamepad prototype is the type GNB 1S 650mAh and is physically 60mm long, 18mm wide, and 8mm high. This battery fits under the ESP32 module.

![GNB Battery](GNB_battery.jpg)

[Link to UK Battery Supplier](https://www.hobbyrc.co.uk/gnb-650mah-1s-60c-lihv-battery-ph20)

![Battery Install](spirotroller_install.jpg)

**WARNING!!** The GNB type batteries are usually available with a 2 pin JST PH 2.0 connector however the pins are usually the wrong way round for the ESP32 module. Reverse polarity will damage the ESP32 module. To reverse the pins the contacts can be removed from the housing by gently lifting the plastic retaining tab on the housing with a pin or needle and withdrawing the contacts. Take care to avoid shorting pins while doing this.

The correct pin orientation is shown below from both sides of the JST connector.

![Battery Pins](battery_pins.jpg)
