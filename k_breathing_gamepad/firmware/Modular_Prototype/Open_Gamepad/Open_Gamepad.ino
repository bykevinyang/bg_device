/*
 * Spirotroller.ino
 * This program is for the Sprirotroller Enhanced with ESP32 contolller
 * It appears as a BLE Gamepad
 * 
 * 
 */
/* includes ************************************************************************************/ 
#include "BleGamepad.h" 
#include "MCP23017_Buttons.h"
#include <Adafruit_BMP280.h>
/* end includes ********************************************************************************/

/* constant defines ****************************************************************************/

/* Hardware IO */
#define START_BUTTON_PIN      15
#define SELECT_BUTTON_PIN     16
#define START_BUTTON_BIT      0
#define SELECT_BUTTON_BIT     1

#define STATUS_LED_PIN        22

#define BATTERY_INPUT_PIN     35 // 35 is on ADC1 so works when bluetooth enabled
#define JOYSTICK_L_V_PIN      36 
#define JOYSTICK_L_H_PIN      39           
#define JOYSTICK_R_V_PIN      33 
#define JOYSTICK_R_H_PIN      34

#define NOSE_PIN_1            18  
#define NOSE_PIN_2                    // 3.3V
#define NOSE_PIN_3            19
#define NOSE_PIN_4            17
#define NOSE_PIN_5            14
#define NOSE_PIN_6            25
#define NOSE_PIN_7            32      // ADC capable 
#define NOSE_PIN_8                    // Ground



#define ADC_STEPS             4095
#define ADC_REFERENCE         3.3    // Volts


/* Sampling settings */
#define TIMESTEP              20

/* Sleep Timeout */
#define SLEEP_TIMEOUT         600000  // 10 minutes


/* NOSE I2C */
#define NOSE_SDA_PIN      NOSE_PIN_4
#define NOSE_SCL_PIN      NOSE_PIN_6
#define BMP280_A__ADDRESS     0x76     
#define BMP280_B__ADDRESS     0x77 

/* Modes */

//#define LOCAL_PRINT 
//#define PRESSURES_PLOT

/* end constant defines ************************************************************************/

/* variable declaration ************************************************************************/

volatile boolean     buttons_changed = false;
volatile uint8_t     STSEL_buttons_state = 0;
uint16_t    MCP_buttons_state = 0;
uint16_t    HID_buttons_state = 0;
int8_t     HID_hat_state;
int8_t    left_x, left_y, right_x, right_y;
uint16_t   battery_voltage;
uint8_t    battery_level;

long      lastTime                    = 0; 
long      currentTime                 = 0; 
long      sleepTime                   = 0;

float pressure_A, pressure_B;
float pressure_A_zero, pressure_B_zero;


TwoWire  WireNose = TwoWire(1);
Adafruit_BMP280 bmp_A = Adafruit_BMP280(&WireNose); 
Adafruit_BMP280 bmp_B = Adafruit_BMP280(&WireNose);
BleGamepad OpenGamepad;
MCP23017_Buttons    MCP_Button_Pad;

/* end variable declaration ********************************************************************/


void IRAM_ATTR Start_Button_isr() {
  buttons_changed = true;
  if(digitalRead(START_BUTTON_PIN) == 0)
  {
    bitSet(STSEL_buttons_state, START_BUTTON_BIT);
  }
  else
  {
    bitClear(STSEL_buttons_state, START_BUTTON_BIT);
  }
}

void IRAM_ATTR Select_Button_isr() {
  buttons_changed = true;
  if(digitalRead(SELECT_BUTTON_PIN) == 0)
  {
    bitSet(STSEL_buttons_state,SELECT_BUTTON_BIT);
  }
  else
  {
    bitClear(STSEL_buttons_state,SELECT_BUTTON_BIT);
  }
}

void IRAM_ATTR MCP23107_INTA_isr() {
  buttons_changed = true;
  sleepTime = millis();
    
}

// Get the initial pressure to convert absolute pressure to guage pressure

void pressure_set_zero(void){
  for(int i = 0; i < 10; i++){
    pressure_A_zero += bmp_A.readPressure();
    pressure_B_zero += bmp_B.readPressure();
    delay(100);
  }
  pressure_A_zero /= 10; // average
  pressure_B_zero /= 10; // average
}


 /**
  * Print output data to serial
  */
void print_readings(){
  
  Serial.print("Buttons Changed: ");
  Serial.print(buttons_changed);
  Serial.print(" Buttons: ");
  Serial.print(HID_buttons_state, HEX);
  Serial.print(" Hat: ");
  Serial.print(HID_hat_state, HEX);
  Serial.print(" Left ");
  Serial.print(left_x);
  Serial.print(" : ");
  Serial.print(left_y);
  Serial.print(" Right ");
  Serial.print(right_x);
  Serial.print(" : ");
  Serial.print(right_y);
  Serial.print(" Zzzz  ");
  Serial.print((currentTime-sleepTime)/1000);
  Serial.print(" Battery  ");
  Serial.println(battery_voltage);
}

/**
  * Print data to serial
  */
void print_pressures(){
  Serial.print(pressure_A);
  Serial.print(" ");
  Serial.println(pressure_B);
  
}

void check_battery(void){
  battery_voltage = map(analogRead(BATTERY_INPUT_PIN), 0, 4095, 0, 7100); // battery voltage in units of 10mV
  OpenGamepad.setBatteryLevel((uint8_t) map(battery_voltage, 3200, 4200, 0, 100));
}

void get_joysticks(void){
  left_x = map(analogRead(JOYSTICK_L_H_PIN), 0, 4095, -127, 127);
  left_y = map(analogRead(JOYSTICK_L_V_PIN), 0, 4095, -127, 127);
  right_x = map(analogRead(JOYSTICK_R_H_PIN), 0, 4095, -127, 127);
  right_y = map(analogRead(JOYSTICK_R_V_PIN), 0, 4095, 127, -127);

}

void sleep_mode(){
  /* Disable Bluetooth */
//  esp_bt_controller_disable();
  /* Enable RTC pullup on Start button */

  /* Wake on Start Button */
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_15, LOW);

  /* Zzzzzz */
  esp_deep_sleep_start();
  
}

void map_buttons(void){
  HID_buttons_state = 0;
  if(MCP_buttons_state & MCP_BUTTON_A ) bitSet(HID_buttons_state, BUTTON_1);
  if(MCP_buttons_state & MCP_BUTTON_B ) bitSet(HID_buttons_state, BUTTON_2);
  if(MCP_buttons_state & MCP_BUTTON_X ) bitSet(HID_buttons_state, BUTTON_3);
  if(MCP_buttons_state & MCP_BUTTON_Y ) bitSet(HID_buttons_state, BUTTON_4);
  if(MCP_buttons_state & MCP_SHOULDER_L ) bitSet(HID_buttons_state, BUTTON_5);
  if(MCP_buttons_state & MCP_SHOULDER_R ) bitSet(HID_buttons_state, BUTTON_6);
  if(STSEL_buttons_state & (1 << START_BUTTON_BIT )) bitSet(HID_buttons_state, BUTTON_7);
  if(STSEL_buttons_state & (1 << SELECT_BUTTON_BIT )) bitSet(HID_buttons_state, BUTTON_8);
  if(MCP_buttons_state & MCP_JOYSTICK_L ) bitSet(HID_buttons_state, BUTTON_9);
  if(MCP_buttons_state & MCP_JOYSTICK_R ) bitSet(HID_buttons_state, BUTTON_10);

  HID_hat_state = 0;
  if(MCP_buttons_state & MCP_DPAD_N ){
    if(MCP_buttons_state & MCP_DPAD_E ) HID_hat_state = DPAD_UP_RIGHT;
    else if(MCP_buttons_state & MCP_DPAD_W ) HID_hat_state = DPAD_UP_LEFT;
    else HID_hat_state = DPAD_UP;
  }
  
  
  if(MCP_buttons_state & MCP_DPAD_S ){
    if(MCP_buttons_state & MCP_DPAD_E ) HID_hat_state = DPAD_DOWN_RIGHT;
    else if(MCP_buttons_state & MCP_DPAD_W ) HID_hat_state = DPAD_DOWN_LEFT;
    else HID_hat_state = DPAD_DOWN;
  }
  

  if(!(MCP_buttons_state & MCP_DPAD_N) && !(MCP_buttons_state & MCP_DPAD_S)){
    if(MCP_buttons_state & MCP_DPAD_E ) HID_hat_state = DPAD_RIGHT;
    if(MCP_buttons_state & MCP_DPAD_W ) HID_hat_state = DPAD_LEFT;
  }
  
}

void setup() {
  Serial.begin(115200);

  

  // Set up the ESP32 GPIO Pins
  pinMode(MCP23017_RESET_PIN, OUTPUT);
  digitalWrite(MCP23017_RESET_PIN, LOW);
  
  pinMode(MCP23017_INTA_PIN, INPUT_PULLUP);
  pinMode(START_BUTTON_PIN, INPUT_PULLUP);
  pinMode(SELECT_BUTTON_PIN, INPUT_PULLUP);

  pinMode(NOSE_PIN_1, INPUT_PULLUP);
  pinMode(NOSE_PIN_3, INPUT_PULLUP);
  pinMode(NOSE_PIN_4, INPUT_PULLUP);
  pinMode(NOSE_PIN_5, INPUT_PULLUP);
  pinMode(NOSE_PIN_6, INPUT_PULLUP);
  pinMode(NOSE_PIN_7, INPUT_PULLUP);
  
  pinMode(STATUS_LED_PIN, OUTPUT);
  digitalWrite(STATUS_LED_PIN, LOW);


  
  // Interrupts for the buttons
  attachInterrupt(START_BUTTON_PIN, Start_Button_isr, CHANGE);
  attachInterrupt(SELECT_BUTTON_PIN, Select_Button_isr, CHANGE);
  attachInterrupt(MCP23017_INTA_PIN, MCP23107_INTA_isr, FALLING);

  MCP_Button_Pad.begin();
  
  // Set up the BMP280s
  WireNose.begin (NOSE_SDA_PIN, NOSE_SCL_PIN);   // 
  
  if (!bmp_A.begin(BMP280_A__ADDRESS)) {
    Serial.println(" BMP Sensor A fail");
    while (1);
  }
  if (!bmp_B.begin(BMP280_B__ADDRESS)) {
    Serial.println(" BMP Sensor B fail");
    while (1);
  }

   /* BMP28 settings from datasheet. */
  bmp_A.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X1,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X4,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_OFF,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_1); /* Standby time. */
  bmp_B.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X1,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X4,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_OFF,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_1); /* Standby time. */
  pressure_set_zero();  //Get the zero pressure for gauge pressure

#ifdef PRESSURES_PLOT
  Serial.println("Wide Narrow");
#endif

  // Start the bluetooth

  OpenGamepad.begin();
}

void loop() {
  currentTime = millis();
  if(currentTime-lastTime > TIMESTEP){
   if(currentTime-sleepTime > SLEEP_TIMEOUT) sleep_mode();
   check_battery();
   MCP_buttons_state = MCP_Button_Pad.readGPIOAB();
   map_buttons();
   get_joysticks();

   pressure_A = bmp_A.readPressure() - pressure_A_zero;
   pressure_B = bmp_B.readPressure() - pressure_B_zero;
   
#ifdef LOCAL_PRINT
    print_readings();
#endif

#ifdef PRESSURES_PLOT
    print_pressures();
#endif

    if(OpenGamepad.isConnected()) {
      
    OpenGamepad.buttons(HID_buttons_state);
    OpenGamepad.setAxes(left_x, left_y, 0, 0, right_x, right_y, HID_hat_state);
        
        
      
      buttons_changed = false;
    }
    lastTime = currentTime;
  }

}
