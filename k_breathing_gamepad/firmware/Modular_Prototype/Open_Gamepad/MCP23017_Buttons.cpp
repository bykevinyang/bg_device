/*!
 * @file MCP23017_Buttons.cpp
 *
 * @mainpage MCP23017 Buttons for Breathing Games Open Gamepad
 *
 * @section intro_sec Introduction
 *
 * 
 *
 * @section license License
 *
 * BSD license, all text above must be included in any redistribution
 */


#include "MCP23017_Buttons.h"



/**
 * Reads a given register
 */
uint8_t MCP23017_Buttons::readRegister(uint8_t addr) {
  // read the register
  Wire.beginTransmission(MCP23017_ADDRESS);
  Wire.write(addr);
  Wire.endTransmission();
  Wire.requestFrom(MCP23017_ADDRESS, 1);
  return Wire.read();
}

/**
 * Writes a given register
 */
void MCP23017_Buttons::writeRegister(uint8_t regAddr, uint8_t regValue) {
  // Write the register
  Wire.beginTransmission(MCP23017_ADDRESS);
  Wire.write(regAddr);
  Wire.write(regValue);
  Wire.endTransmission();
}



////////////////////////////////////////////////////////////////////////////////

/*!
 * Initializes the MCP23017 given its HW selected address, see datasheet for
 * Address selection.
*/
void MCP23017_Buttons::begin(void) {
  Wire.begin (MCP23017_SDA_PIN,MCP23017_SCL_PIN );  

  // Reset MCP23017
  pinMode(MCP23017_RESET_PIN, OUTPUT);
  digitalWrite(MCP23017_RESET_PIN, HIGH);

  // IO Configuration
  writeRegister(MCP23017_IOCONA, 0x44);   // default except MIRROR and open drain
  writeRegister(MCP23017_IOCONB, 0x44);   // default except MIRROR and open drain
  
  // all inputs on port A and B
  writeRegister(MCP23017_IODIRA, 0xff);
  writeRegister(MCP23017_IODIRB, 0xff);

  // Invert the inputs
  writeRegister(MCP23017_IPOLA, 0xff);
  writeRegister(MCP23017_IPOLB, 0xff);

  // Enable Pullups
  writeRegister(MCP23017_GPPUA, 0xff);
  writeRegister(MCP23017_GPPUB, 0xff);

  // Setup Interrupts
  writeRegister(MCP23017_GPINTENA, 0x0f); // Interrupt on change port A pins 0:3
  writeRegister(MCP23017_GPINTENB, 0xff); // Interrupt on change port B pins 0:7

  
}



/**
 * Reads all 16 pins (port A and B) into a single 16 bits variable.
 * @return Returns the 16 bit variable representing all 16 pins
 */
uint16_t MCP23017_Buttons::readGPIOAB() {
  uint16_t ba = 0;
  uint8_t a;

  
  // read the current GPIO output latches
  Wire.beginTransmission(MCP23017_ADDRESS);
  Wire.write(MCP23017_GPIOA);
  Wire.endTransmission();

  Wire.requestFrom(MCP23017_ADDRESS, 2);
  a = Wire.read();
  ba = Wire.read();
  ba <<= 8;
  ba |= a;

  readRegister(MCP23017_INTCAPA);
  readRegister(MCP23017_INTCAPB);

  return ba;
}
