# Hardware devices
Hardware for health co-created by commonners of [Breathing Games](http://www.breathinggames.net), and released under free/libre licence.

Developed for [Arduino](https://www.arduino.cc), to be used on a [breathing device](https://gitlab.com/breathinggames/bg/wikis/5-hardware).

**Read the [documentation](https://gitlab.com/breathinggames/bg_device/wikis/home).**

Watch the device conception on [YouTube](https://www.youtube.com/watch?v=U4s3kpndVR8).

Designs are based on variety of hardware devices including:
- Adafruit Bluefruit 32U4
- Adafruit Huzzah ESP32
- Custom ESP32 Breathing Gamepad Hardware

## In short
...

## For Adafruit BlueFruit 32U4

### BOM

[Adafruit Feather 32u4 Bluefruit LE](https://www.adafruit.com/product/2829) 

### Setup
#### Software
[Adafruit Feather 32u4 Bluefruit LE Overview](https://learn.adafruit.com/adafruit-feather-32u4-bluefruit-le)
[Feather help!](https://learn.adafruit.com/adafruit-feather-32u4-adalogger/faq)
#### Schematic
[Schematic](https://gitlab.com/breathinggames/bg_device/blob/master/Firmware/BG_sensors_calibration/Calibration_setup.pdf)


## Contributors
Contributors are welcome. To join, contact info (at) breathinggames.net

- Jim Anastassio
- Matthew Brown
- Tiberius Brastaviceanu
- Richard Ibbotson
- Povilas Jurgaitis
- Emmanuel Kellner
- Humberto Quintana
- Andres Romero Vasquez
- Kevin Yang



