These firmware were designed to be used with the Adafruit Feather with BLE module: 


HARDWARE SETUP: 


Adafruit Feather with BLE module: 

	https://learn.adafruit.com/adafruit-feather-32u4-bluefruit-le

Additionally we used the MPXV5010DP 

	https://www.nxp.com/docs/en/data-sheet/MPX5010.pdf





Using the FeatherWing Proto board:
	https://www.adafruit.com/product/2884
	

We attached the MPXV5010DP using the following pinout configuration. 

	SensorPin2 to 5V
	SensorPin3 to Ground
	SensorPin4 to A0 


We added a TACT switch to enable recording: 

	https://www.amazon.ca/Gikfun-6x6x5mm-Switch-Arduino-EK1019C/dp/B06Y6DDG8K


The TACT switch is connected

	GND to Pin12 



The Recording LED is connected 

	Pin11 to GND using a 500 Ohm resistor 









