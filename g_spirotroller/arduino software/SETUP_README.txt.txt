SOFTWARE: 


SERIAL COMUNICATION
========================================================================================
SpiroMeter.ino 

This firmware can be used to transmit data between the Spirometer and attached computer using serial communication. 

------------------------------------------------------------------
Setup requirements: 

Arduino IDE (Tested with version 1.8.19.0)
Processing 3 (Tested with version 3.3.6)
SpiroMeter.ino
FlowTestSpiro.pde

------------------------------------------------------------------

To run the device sing the Spirometer.ino Serial Communication: 

1) Flash the Feather with the firmware titled "SpiroMeter.ino"

2) Connect the Feather module to a computer using the micro USB port. The device is setup to transmit data at a baudrate of 9600 bps. This will initialize the device. 

3) Open the corresponding file in the TestSoftware directory called "FlowTestSpiro". This may require downloading Processing 3 if it is not downloaded. 

4) Run the FlowTestSpiro application. 

5) Press the button on the control board and exhale into the controller. The flow rate will be measured and displayed on a graph. When finished blowing release the button. This will display the total volume of the exhaled air. 

------------------------------------------------------------------

Passed Data structure: 

The Spirometer is configured to output data in the form of an ASCII string in the form: 

In the base configuration for this firmware the output variables are: 

[volume], [flow rate], [volume recorded] \n



BLUETOOTH COMUNICATION
========================================================================================


Spirometer_BLE.ino

This firmware is used to transmit data between the Spirometer and an attached device over a Bluetooth LE connection. 

------------------------------------------------------------------
Setup requirements: 

Arduino IDE (Tested with version 1.8.19.0)
AdaFruit BlueFruit LE Connect(Tested with version 3.3.6)
SpiroMeter_BLE.ino

------------------------------------------------------------------

To run the device sing the Spirometer_BLE.ino Bluetooth Communication: 

1) Flash the Feather with the firmware titled "SpiroMeter_BLE.ino"

2) Connect the Feather module to a computer using the micro USB port or power it through the micro USB port with a 5V powersupply (a cellphone charger or powerbank will work). The device is setup to transmit data over Bluetooth at every 100 ms. This will initialize the device. 

3) Download the application from the Android webstore called AdaFruit BlueFruit LE Connect. This will act as a reciever to test the Bluetooth communication. 

https://play.google.com/store/apps/details?id=com.adafruit.bluefruit.le.connect

4) Run the AdaFruit BlueFruit LE Connect application. In the application connect to the bluetooth device named "Adafruit Bluefruiet LE" or "Breathing Games". 

5) Once connected to the device open up the UART pannel. This will start receiving data. 

6) Press the button on the control board and exhale into the controller. The flow rate will be measured and displayed on a graph. When finished blowing release the button. This will display the data passed to the cell phone. 

------------------------------------------------------------------

Passed Data structure: 

The Spirometer is configured to output data in the form of an ASCII string in the form: 

In the base configuration for this firmware the output variables are: 

[volume], [flow rate], [volume recorded] \n


Adafruit blufruit LEConnect



