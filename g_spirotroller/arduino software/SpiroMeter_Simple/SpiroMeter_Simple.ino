/**
 ************************************************************************************************
 * @file       SpiroMeter_BLE.ino
 * @author     Colin Gallacher, Steve Ding
 * @version    V1.0.0
 * @date       10-December-2018
 * @brief      Adafruit Feather 32u4 Bluefruit LE Spirometer control code for flow rate 
 *             recording and BLE data transmission
 ************************************************************************************************
 * @attention
 *
 *
 ************************************************************************************************
 */

/* includes ************************************************************************************/
#include <Arduino.h>
#include <SPI.h>

#include <MegunoLink.h>
#include <Filter.h>

#include "Adafruit_BLE.h"
#include "Adafruit_BluefruitLE_SPI.h"
#include "Adafruit_BluefruitLE_UART.h"

#include "BluefruitConfig.h"

#if not defined (_VARIANT_ARDUINO_DUE_X_) && not defined (_VARIANT_ARDUINO_ZERO_)
  #include <SoftwareSerial.h>
#endif
/* end includes ********************************************************************************/


/* constant defines ****************************************************************************/
#define   TIMESTEP                    50
#define   TRANSMITTIMESTEP            100

#define   ANALOG_IN                   A0
#define   RECORD_BUTTON               12
#define   RECORD_LED                  11

#define   AREA1                       0.001 //m^2
#define   AREA2                       0.0005 //m^2
#define   AIR_DENSITY                 1.225 //kg/m^3
#define   DENOMINATOR()               (((AREA1/AREA2) * (AREA1/AREA2)) - 1)

#define   OFFSET                      69
#define   CORRECTIONFACTOR            0.15

#define   FACTORYRESET_ENABLE         1
#define   MINIMUM_FIRMWARE_VERSION    "0.6.6"
#define   MODE_LED_BEHAVIOUR          "MODE"
#define   DEVICE_NAME                 "Breathing Games"
/* end constant defines ************************************************************************/


/* variable declaration ************************************************************************/
long      lastTime                    = 0; 
long      lastTransmitTime            = 0;
long      currentTime                 = 0; 

boolean   wasRecording                = false; 

float     oldPressureDiffKPA          = 0.0f;
float     pressureDiffKPA;
float     flowRate;
 
float     volume                      = 0.0f; 
float     volumeRecorded              = 0.0f; 

ExponentialFilter<long> ADCFilter(60, 0);
Adafruit_BluefruitLE_SPI ble(BLUEFRUIT_SPI_CS, BLUEFRUIT_SPI_IRQ, BLUEFRUIT_SPI_RST);
/* end variable declaration ********************************************************************/


/* BLE functions *******************************************************************************/
/* A small helper */
void error(const __FlashStringHelper*err) {
  Serial.println(err);
  while (1);
}

/* Initialize BLE */
void init_BLE(){
  Serial.println(F("Adafruit Bluefruit Command Mode Example"));
  Serial.println(F("---------------------------------------"));

  /* Initialise the module */
  Serial.print(F("Initialising the Bluefruit LE module: "));

  if ( !ble.begin(VERBOSE_MODE) )
  {
    error(F("Couldn't find Bluefruit, make sure it's in CoMmanD mode & check wiring?"));
  }
  Serial.println( F("OK!") );

  if ( FACTORYRESET_ENABLE )
  {
    /* Perform a factory reset to make sure everything is in a known state */
    Serial.println(F("Performing a factory reset: "));
    if ( ! ble.factoryReset() ){
      error(F("Couldn't factory reset"));
    }
  }

  /* Disable command echo from Bluefruit */
  ble.echo(false);

  Serial.println("Requesting Bluefruit info:");
  /* Print Bluefruit information */
  ble.info();

  Serial.println(F("Please use Adafruit Bluefruit LE app to connect in UART mode"));
  Serial.println(F("Then Enter characters to send to Bluefruit"));
  Serial.println();

  ble.verbose(false);  // debug info is a little annoying after this point!

  /* Wait for connection */
  while (! ble.isConnected()) {
      delay(500);
  }

  // LED Activity command is only supported from 0.6.6
  if ( ble.isVersionAtLeast(MINIMUM_FIRMWARE_VERSION) )
  {
    // Change Mode LED Activity
    Serial.println(F("******************************"));
    Serial.println(F("Change LED activity to " MODE_LED_BEHAVIOUR));
    ble.sendCommandCheckOK("AT+HWModeLED=" MODE_LED_BEHAVIOUR);
    ble.sendCommandCheckOK("AT+GAPDEVNAME=" DEVICE_NAME);
    Serial.println(F("******************************"));
  }
}

/* send ble data */
void send_serial_ble(){
  ble.print("AT+BLEUARTTX=");
  ble.print(volume * 1000 * CORRECTIONFACTOR); //m^3 to liters
  ble.print(";");
  ble.print(flowRate * 60000 * CORRECTIONFACTOR);//m^3/s to liters/minute
  ble.print(";");
  ble.println(volumeRecorded);

  if (! ble.waitForOK() ) {
    Serial.println(F("Failed to send?"));
  }
}
/* End BLE functions ***************************************************************************/


/* pressure calculation functions **************************************************************/
 /**
  * Calculate pressure difference in KPA based on analog signal input
  *
  * @param    analogPin analog input pin
  * @return   pressure difference in KPA
  */
float pressure_difference_kpa(int analogPin){
  
  int diff = analogRead(analogPin);
  
  ADCFilter.Filter(diff);
  
  float offsetMv = OFFSET * 3.2; //10bit;
  float diff2mv = ADCFilter.Current() * 3.2;
  float pressureDiff = (diff2mv - offsetMv) / 450; //kpa

  pressureDiff = (pressureDiff < .1) ? 0 : pressureDiff;

  return pressureDiff;
}


 /**
  * Calculate flow rate based on recorded pressure
  *
  * @param    pressure pressure in KPA
  * @return   flow rate
  */
float get_flow_rate(float pressure){
  
  float numerator = 2 * pressure * 1000 / AIR_DENSITY;
  float flow = AREA1 * sqrt(numerator/DENOMINATOR());

  return flow;
}
/* end pressure calculation functions **********************************************************/


/* recording and display functions *************************************************************/
 /**
  * Record volume readings based on button click
  *
  * @param    record_button button pin to initialize recording
  */
void record_reading(int record_button){

  switch(!digitalRead(record_button)){
    case 0:
      digitalWrite(RECORD_LED, LOW);
      if(wasRecording){
        volumeRecorded = volume;
        volume = 0.0f;
      }
      wasRecording = false;
      break;
      
    case 1:
      volume = volume + flowRate * TIMESTEP / 1000;
      digitalWrite(RECORD_LED, HIGH);
      wasRecording = true;
      break; 
  }
}


 /**
  * Print data to serial
  */
void print_readings(){
 // Serial.print(volume * 1000 * CORRECTIONFACTOR); //m^3 to liters
 // Serial.print(";");
  Serial.println(flowRate * 4000 * CORRECTIONFACTOR);//m^3/s to 0.1 liters/minute
 // Serial.print(";");
 // Serial.println(volumeRecorded);
}
/* end recording and display functions *********************************************************/



/***********************************************************************************************
 * Main setup function, defines parameters and hardware setup
 ***********************************************************************************************/
void setup() {
  Serial.begin(9600); 
  
//  init_BLE();
  
  pinMode(RECORD_BUTTON, INPUT_PULLUP); 
  pinMode(RECORD_LED, OUTPUT);   
}
/***********************************************************************************************
 * End main setup function, defines parameters and hardware setup
 ***********************************************************************************************/



/***********************************************************************************************
 * Main loop function
 ***********************************************************************************************/
void loop() {
  
  currentTime = millis();
  
  // record data at a rate of 50ms
  if(currentTime-lastTime > TIMESTEP){
    
    pressureDiffKPA = pressure_difference_kpa(ANALOG_IN);

    flowRate = get_flow_rate(pressureDiffKPA);
    
    record_reading(RECORD_BUTTON);

    oldPressureDiffKPA = pressureDiffKPA;

    print_readings();
    
    lastTime = currentTime;
  }


//  // send data over ble at a rate of 100ms when button is pressed
//  if(currentTime - lastTransmitTime > TRANSMITTIMESTEP){
//
//    if(!digitalRead(RECORD_BUTTON)){
//      send_serial_ble();
//    }
//    lastTransmitTime = currentTime;
//  }
}
/***********************************************************************************************
 * End main loop function
 ***********************************************************************************************/
