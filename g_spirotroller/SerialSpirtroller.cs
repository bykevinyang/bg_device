﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;

public class SerialSpirtroller : InputController_I
{

    [SerializeField]
    public string PortName { get; set; }

    [SerializeField]
    protected int BaudRate { get; set; }
    [SerializeField]
    protected int serialTimeout;

    public SerialPort Port { get; protected set; }

    private float StrongBreathValue { get; set; }
    private float CalibrationFactor { get; set; }

    public static string arduinoValue;

    protected bool hasBeenInitialized;

    public SerialSpirtroller(float strongBreathValue, string portName, int baudRate)
    {
        this.PortName = portName;
        this.BaudRate = baudRate;
        serialTimeout = 100; 

        this.StrongBreathValue = strongBreathValue;

        this.Initialize(); 
    }

    //Class methods 

    public virtual void Initialize()
    {
        if (hasBeenInitialized)
        {
            Debug.Log("Board Already Initialized");
            return;
        }

        try
        {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            Port = new SerialPort(PortName, BaudRate);
#else
		port = new SerialPort(PortName, baudRate);
#endif
            Port.ReadTimeout = serialTimeout;
            Port.WriteTimeout = serialTimeout;
            Port.DtrEnable = true;
            Port.RtsEnable = true;


            Port.Open();
            Debug.Log("Initialized Board");
            Debug.Log(Port.IsOpen);

            hasBeenInitialized = true;
        }
        catch (System.Exception exception)
        {
            Debug.LogException(exception);
        }
    }

    public virtual void ClosePort()
    {
        Port.Close();

        hasBeenInitialized = false;
        Debug.Log("Port closed");
    }

    public virtual bool Data_available()
    {
        bool available = false;

        if (Port.BytesToRead > 0)
        {
            available = true;
        }

        return available;
    }


    private void Set_buffer(int length)
    {
        Port.ReadBufferSize = length;
    }



    //Interface methods 

    void InputController_I.Update()
    {
        Debug.Log(Port.IsOpen.ToString());


        if (Port.IsOpen)
        { // stream.available() > 0; ?
            try
            {
                // reads serial port
                arduinoValue = Port.ReadLine();

                this.StrongBreathValue = float.Parse(arduinoValue);
                //	this.strongBreathValue = (this.strongBreathValue - 80) / 160;
            }
            catch (System.Exception)
            {

            }
        }
        Debug.Log("ard " + arduinoValue);
        Debug.Log("sbv " + this.StrongBreathValue);
       
    }

    public float GetStrength()
    {
        return StrongBreathValue;
    }

    public BreathingState GetInputState()
    {
        if (this.StrongBreathValue > 0.2f)
        {
            return BreathingState.EXPIRATION;
        }
        else
        {
            return BreathingState.INSPIRATION; // HOLDING_BREATH
        }
    }

    

    public bool IsMoving()
    {
        return Input.GetKey(KeyCode.RightArrow);
    }


    public void SetCalibrationFactor(float calibrationFactor)
    {
        this.CalibrationFactor = calibrationFactor;
    }

    public float GetCalibrationFactor()
    {
        return CalibrationFactor;
    }



    public float GetMovingDirection()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            return 1f;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            return -1f;
        }
        return 0f;
    }

    void OnDisable()
    {
        this.ClosePort(); 

    }


}
