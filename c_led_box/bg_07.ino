/*


 */

#include <Arduino.h>
#include <SPI.h>
#if not defined (_VARIANT_ARDUINO_DUE_X_) && not defined (_VARIANT_ARDUINO_ZERO_)
  #include <SoftwareSerial.h>
#endif

#include "Adafruit_BLE.h"
#include "Adafruit_BluefruitLE_SPI.h"
#include "Adafruit_BluefruitLE_UART.h"

#include "BluefruitConfig.h"

/*=========================================================================
    APPLICATION SETTINGS

    FACTORYRESET_ENABLE       Perform a factory reset when running this sketch
   
                              Enabling this will put your Bluefruit LE module
                              in a 'known good' state and clear any config
                              data set in previous sketches or projects, so
                              running this at least once is a good idea.
   
                              When deploying your project, however, you will
                              want to disable factory reset by setting this
                              value to 0.  If you are making changes to your
                              Bluefruit LE device via AT commands, and those
                              changes aren't persisting across resets, this
                              is the reason why.  Factory reset will erase
                              the non-volatile memory where config data is
                              stored, setting it back to factory default
                              values.
       
                              Some sketches that require you to bond to a
                              central device (HID mouse, keyboard, etc.)
                              won't work at all with this feature enabled
                              since the factory reset will clear all of the
                              bonding data stored on the chip, meaning the
                              central device won't be able to reconnect.
    MINIMUM_FIRMWARE_VERSION  Minimum firmware version to have some new features
    MODE_LED_BEHAVIOUR        LED activity, valid options are
                              "DISABLE" or "MODE" or "BLEUART" or
                              "HWUART"  or "SPI"  or "MANUAL"
    -----------------------------------------------------------------------*/
    #define FACTORYRESET_ENABLE         1
    #define MINIMUM_FIRMWARE_VERSION    "0.6.6"
    #define MODE_LED_BEHAVIOUR          "MODE"
    #define DEVICE_NAME                 "Breathing Games"
/*=========================================================================*/

// Create the bluefruit object, either software serial...uncomment these lines
/*
SoftwareSerial bluefruitSS = SoftwareSerial(BLUEFRUIT_SWUART_TXD_PIN, BLUEFRUIT_SWUART_RXD_PIN);

Adafruit_BluefruitLE_UART ble(bluefruitSS, BLUEFRUIT_UART_MODE_PIN,
                      BLUEFRUIT_UART_CTS_PIN, BLUEFRUIT_UART_RTS_PIN);
*/

/* ...or hardware serial, which does not need the RTS/CTS pins. Uncomment this line */
// Adafruit_BluefruitLE_UART ble(Serial1, BLUEFRUIT_UART_MODE_PIN);

/* ...hardware SPI, using SCK/MOSI/MISO hardware SPI pins and then user selected CS/IRQ/RST */
Adafruit_BluefruitLE_SPI ble(BLUEFRUIT_SPI_CS, BLUEFRUIT_SPI_IRQ, BLUEFRUIT_SPI_RST);

/* ...software SPI, using SCK/MOSI/MISO user-defined SPI pins and then user selected CS/IRQ/RST */
//Adafruit_BluefruitLE_SPI ble(BLUEFRUIT_SPI_SCK, BLUEFRUIT_SPI_MISO,
//                             BLUEFRUIT_SPI_MOSI, BLUEFRUIT_SPI_CS,
//                             BLUEFRUIT_SPI_IRQ, BLUEFRUIT_SPI_RST);


// A small helper
void error(const __FlashStringHelper*err) {
  //Serial.println(err);
  while (1);
}

/////////////////// LED
#include <Wire.h>
#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"

Adafruit_BicolorMatrix matrix = Adafruit_BicolorMatrix();


unsigned int long PRINT_DELAY = 10000;
unsigned int long last_print_time = 0;
unsigned int long last_print_time_2 = 0;

int long total_calibration = 0;
int total_calibration_number = 2000;


int column_1 = 0;

const int numReadings = 10;

int readings[numReadings];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
int total = 0;                  // the running total
int average = 0;                // the average



// These constants won't change.  They're used to give names
// to the pins used:
const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to

int sensorValue = 0;        // value read from the pot
int outputValue = 0;
int LEDValue = 0;

int min_sensor = 385;
int max_sensor = 419;

#define print_delay() delay(2);

void setup() {

  
  // initialize serial communications at 115200 bps:
  Serial.begin(115200);
//  Serial.println(F("Adafruit Bluefruit Command Mode Example"));
//  Serial.println(F("---------------------------------------"));

  /* Initialise the module */
  //Serial.print(F("Initialising the Bluefruit LE module: "));

  if ( !ble.begin(VERBOSE_MODE) )
  {
    error(F("Couldn't find Bluefruit, make sure it's in CoMmanD mode & check wiring?"));
  }
  //Serial.println( F("OK!") );

  if ( FACTORYRESET_ENABLE )
  {
    /* Perform a factory reset to make sure everything is in a known state */
    //Serial.println(F("Performing a factory reset: "));
    if ( ! ble.factoryReset() ){
      error(F("Couldn't factory reset"));
    }
  }

  /* Disable command echo from Bluefruit */
  ble.echo(false);

  //Serial.println("Requesting Bluefruit info:");
  /* Print Bluefruit information */
  ble.info();


    ble.verbose(false);  // debug info is a little annoying after this point!

  /* Wait for connection */
//  while (! ble.isConnected()) {
//      delay(500);
//  }

  // LED Activity command is only supported from 0.6.6
  if ( ble.isVersionAtLeast(MINIMUM_FIRMWARE_VERSION) )
  {
    // Change Mode LED Activity
    //Serial.println(F("******************************"));
    //Serial.println(F("Change LED activity to " MODE_LED_BEHAVIOUR));
    ble.sendCommandCheckOK("AT+HWModeLED=" MODE_LED_BEHAVIOUR);
    ble.sendCommandCheckOK("AT+GAPDEVNAME=" DEVICE_NAME);
<<<<<<< HEAD:v0.7_new/bg_07/bg_07.ino
    Serial.println(F("******************************"));
=======
    //Serial.println(F("******************************"));
>>>>>>> master:v0.7_new/bg_07.ino
  }

  for (int thisReading = 0; thisReading < numReadings; thisReading++) {
    readings[thisReading] = 0;
  }
  //analogReference(DEFAULT);
  //analogReference(INTERNAL);
  analogReference(EXTERNAL);
  matrix.begin(0x70);  // pass in the address
  get_calibration();
}

static const uint8_t PROGMEM
  smile_bmp[] =
  { B00111100,
    B01000010,
    B10100101,
    B10000001,
    B10100101,
    B10011001,
    B01000010,
    B00111100 },
  neutral_bmp[] =
  { B00111100,
    B01000010,
    B10100101,
    B10000001,
    B10111101,
    B10000001,
    B01000010,
    B00111100 },
  frown_bmp[] =
  { B00111100,
    B01000010,
    B10100101,
    B10000001,
    B10011001,
    B10100101,
    B01000010,
    B00111100 };

    
void get_calibration(){
  int i;
  for(i=0;i<total_calibration_number;i++){
    sensorValue = analogRead(analogInPin);
    // subtract the last reading:
    total = total - readings[readIndex];
    // read from the sensor:
    readings[readIndex] = analogRead(analogInPin);
    // add the reading to the total:
    total = total + readings[readIndex];
    // advance to the next position in the array:
    readIndex = readIndex + 1;
    // if we're at the end of the array...
    if (readIndex >= numReadings) {
      // ...wrap around to the beginning:
      readIndex = 0;
    }
    // calculate the average:
    average = total / numReadings;
    total_calibration = total_calibration + average;
    print_delay();
  }
  min_sensor = (total_calibration / total_calibration_number);

}

void get_readings(){
    // read the analog in value:
  sensorValue = analogRead(analogInPin);
  // subtract the last reading:
  total = total - readings[readIndex];
  // read from the sensor:
  readings[readIndex] = analogRead(analogInPin);
  // add the reading to the total:
  total = total + readings[readIndex];
  // advance to the next position in the array:
  readIndex = readIndex + 1;
  // if we're at the end of the array...
  if (readIndex >= numReadings) {
    // ...wrap around to the beginning:
    readIndex = 0;
  }
  // calculate the average:
  average = total / numReadings;
  // send it to the computer as ASCII digits
  // map it to the range of the analog out:
  outputValue = map(average, min_sensor, max_sensor, 0, 1000);
  LEDValue= map(average, min_sensor, max_sensor, 0, 7);
}

void printout_serial_ble(){
  get_readings();
  
  Serial.println(outputValue);
  ble.print("AT+BLEUARTTX=");
  ble.println(outputValue);
  // check response stastus
//  if (! ble.waitForOK() ) {
//    Serial.println(F("Failed to send?"));
//  }

}

void loop() {
  int i,j;
  
  
  // change the analog out value:
  // print the results to the serial monitor:
//  Serial.print("min_sensor = ");
//  Serial.print(min_sensor);
//  Serial.print(" ");
//  Serial.print("sensor = ");
//  Serial.print(average);
//  Serial.print(" ");
//  Serial.print("output = ");
  printout_serial_ble();

if (millis() - last_print_time >= 3000){
  //3 seconds passed, do the countdown on the led matrix. 3, 2, 1, 
  for(i=3;i>=1;i--){
    matrix.setTextWrap(false);  // we dont want text to wrap so it scrolls nicely
    matrix.setTextSize(1);
    matrix.setTextColor(LED_RED);
    matrix.clear();
    matrix.setCursor(2,1);
    matrix.print(i);
    matrix.writeDisplay();
    
    last_print_time_2 = millis();
    do{
      printout_serial_ble();
      print_delay();
    }while(millis() - last_print_time_2 <= 1000);
  }
  
  last_print_time = millis();
  matrix.clear();

  for (j=0; j<=7; j++){
    do{
      printout_serial_ble();
      matrix.drawLine(j,7,j,7,LED_GREEN);
      matrix.drawLine(j,7-LEDValue,j,7, LED_YELLOW);
      matrix.writeDisplay();  // write the changes we just made to the display
      print_delay();      
    }while(millis() - last_print_time <= (500 + 500 * j));
  }

  if (LEDValue > 3){
    matrix.clear();
    matrix.drawBitmap(0, 0, smile_bmp, 8, 8, LED_GREEN);
    matrix.writeDisplay();
  }else if((LEDValue ==3)||(LEDValue==2)){
    matrix.clear();
    matrix.drawBitmap(0, 0, neutral_bmp, 8, 8, LED_YELLOW);
    matrix.writeDisplay();
  }else if(LEDValue<2){
    matrix.clear();
    matrix.drawBitmap(0, 0, frown_bmp, 8, 8, LED_RED);
    matrix.writeDisplay();    
  }

  last_print_time = millis();
  do{
    printout_serial_ble();
    print_delay();
  }while(millis() - last_print_time <= 2000);
  last_print_time = millis();  
}


  


  
}



/////////////////////////// EXAMPLES FOR MATRIX

//  matrix.clear();
//  matrix.drawBitmap(0, 0, smile_bmp, 8, 8, LED_GREEN);
//  matrix.writeDisplay();
//  delay(500);
//
//  matrix.clear();
//  matrix.drawBitmap(0, 0, neutral_bmp, 8, 8, LED_YELLOW);
//  matrix.writeDisplay();
//  delay(500);
//
//  matrix.clear();
//  matrix.drawBitmap(0, 0, frown_bmp, 8, 8, LED_RED);
//  matrix.writeDisplay();
//  delay(500);
//
//  matrix.clear();      // clear display
//  matrix.drawPixel(0, 0, LED_GREEN);  
//  matrix.writeDisplay();  // write the changes we just made to the display
//  delay(500);
//
//  matrix.clear();
//  matrix.drawLine(0,0, 7,7, LED_YELLOW);
//  matrix.writeDisplay();  // write the changes we just made to the display
//  delay(500);
//
//  matrix.clear();
//  matrix.drawRect(0,0, 8,8, LED_RED);
//  matrix.fillRect(2,2, 4,4, LED_GREEN);
//  matrix.writeDisplay();  // write the changes we just made to the display
//  delay(500);
//
//  matrix.clear();
//  matrix.drawCircle(3,3, 3, LED_YELLOW);
//  matrix.writeDisplay();  // write the changes we just made to the display
//  delay(500);
//
//  matrix.setTextWrap(false);  // we dont want text to wrap so it scrolls nicely
//  matrix.setTextSize(1);
//  matrix.setTextColor(LED_GREEN);
//  for (int8_t x=7; x>=-36; x--) {
//    matrix.clear();
//    matrix.setCursor(x,0);
//    matrix.print("Hello");
//    matrix.writeDisplay();
//    delay(100);
//  }
//  matrix.setRotation(3);
//  matrix.setTextColor(LED_RED);
//  for (int8_t x=7; x>=-36; x--) {
//    matrix.clear();
//    matrix.setCursor(x,0);
//    matrix.print("World");
//    matrix.writeDisplay();
//    delay(100);
//  }
//  matrix.setRotation(0);
