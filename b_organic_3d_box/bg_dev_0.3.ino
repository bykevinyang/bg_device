/************************************************************
 *                       Breathing Games                    *
 *                                                          *
 *           Peripheral device sketch on Arduino Nano       *
 *             written by Jim Anastassiou     v 0.2         *
 ************************************************************/

int sensorPin = A5;     // select the input pin for the pressure sensor
int sensorValue = 0;    // variable to store the value coming from the sensor
int scaledPressure = 0; // the output of the device remapped from 1 - 100
int pinBrightness = 0;  // pin brightness variable
int ledPin = 9;         // the pin that the LED is attached to

void setup()
{
  Serial.begin(9600);  
  pinMode(ledPin, OUTPUT);      // sets the digital pin as output
}

void loop() {
  
// read the value from the sensor:
sensorValue = analogRead(sensorPin); 
scaledPressure = constrain(map(sensorValue,270,820,0,100),0,100);
//Serial.print(sensorValue);
//Serial.print(",");
Serial.println(scaledPressure);
//Serial.print(",");
//Serial.println(pinBrightness);
switch (scaledPressure){
  case 0 ... 10:
    pinBrightness = 0;
    break;
  case 11 ... 25:
    pinBrightness = 15;
    break;
  case 26 ... 50:
    pinBrightness = 50;
    break;
  case 51 ... 75:
    pinBrightness = 125;
    break;
  case 76 ... 100:
    pinBrightness = 255;
    break;
  }
analogWrite(ledPin,pinBrightness); 
delay(10);

}
