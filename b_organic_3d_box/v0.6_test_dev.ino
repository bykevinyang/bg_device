/************************************************************
 *                       Breathing Games                    *
 *                                                          *
 *           Peripheral device sketch on Arduino Nano       *
 *             written by Jim Anastassiou     v 0.4         *
 ************************************************************/

#include <Adafruit_BLE_UART.h>
#include <SPI.h>
#include <SD.h>

int sensorPin = A5;            // input pin for the pressure sensor
int ledPin = 3;                // the pin that the LED is attached to

int sensorValue = 0;           // raw sensor value 
int scaledPressure = 0;        // the output of the device remapped from 1 - 100
String scaledPressureString;   // converted to string
int pinBrightness = 0;         // LED brightness 


// *******************************************************************BLE Module
// Connect CLK/MISO/MOSI to hardware SPI
// On NANO: CLK = 13, MISO = 12, MOSI = 11
#define ADAFRUITBLE_REQ 10
#define ADAFRUITBLE_RDY 2     // This should be an interrupt pin, on Uno thats #2 or #3
#define ADAFRUITBLE_RST 9
Adafruit_BLE_UART BTLEserial = Adafruit_BLE_UART(ADAFRUITBLE_REQ, ADAFRUITBLE_RDY, ADAFRUITBLE_RST);


// **********************************************************************SD card
// Connect CLK/DO/DI to hardware SPI
// On NANO: CLK = 13, DO = 12, DI = 11
// CS must be different than other devices
const int chipSelect = 9; 


//********************************************************************begin setup
void setup(){
  
    Serial.begin(9600);  
    pinMode(ledPin, OUTPUT);      // sets the LED pin as output

    BTLEserial.setDeviceName("BG_DEV"); // BLE device name 7 characters max! 
    BTLEserial.begin();                 // begin advertising    
    
    Serial.print("Initializing SD card...");
    // see if the card is present and can be initialized:
    if (!SD.begin(chipSelect)) {
        Serial.println("Card failed, or not present");
        // don't do anything more:
        return;
    }
    Serial.println("card initialized.");
} 
//********************************************************************end setup


aci_evt_opcode_t laststatus = ACI_EVT_DISCONNECTED; // why isn't this flag initialized before setup?!

//**************************************************************begin main loop
void loop() {

    //*********************************************************Sensor
    // Get sensor value and re-map from 0-100
    sensorValue = analogRead(sensorPin); 
    scaledPressure = constrain(map(sensorValue,270,820,0,100),0,100);
    scaledPressureString = String(scaledPressure);
    Serial.println(scaledPressureString);
    
    // Keep for debugging
    //Serial.print(sensorValue);
    //Serial.print(",");
    //Serial.println(pinBrightness);

    //***********************************************************LED
    // Adjust LED brightness to re-mapped sensor input for feedback 
    switch (scaledPressure){
        case 0 ... 10:
            pinBrightness = 0;
            break;
        case 11 ... 25:
            pinBrightness = 15;
            break;
        case 26 ... 50:
            pinBrightness = 50;
            break;
        case 51 ... 75:
            pinBrightness = 125;
            break;
        case 76 ... 100:
            pinBrightness = 255;
            break;
    }
    analogWrite(ledPin,pinBrightness); 

    //****************************************************Bluetooth

    // Tell the nRF8001 to do whatever it should be working on.
    BTLEserial.pollACI();
    // Ask what is our current status
    aci_evt_opcode_t status = BTLEserial.getState();
    // If the status changed....
    if (status != laststatus) {
        // print it out!
        if (status == ACI_EVT_DEVICE_STARTED) {
            Serial.println(F("* Advertising started"));
        }
        if (status == ACI_EVT_CONNECTED) {
            Serial.println(F("* Connected!"));
        }
        if (status == ACI_EVT_DISCONNECTED) {
            Serial.println(F("* Disconnected or advertising timed out"));
        }
        // OK set the last status change to this one
        laststatus = status; 
    }

    // Send over bluetooth if connected
    if (status == ACI_EVT_CONNECTED) {
        uint8_t sendbuffer[20];
        scaledPressureString.getBytes(sendbuffer, 20);
        char sendbuffersize = min(20, scaledPressureString.length());
        Serial.print(F("\n* Sending -> \"")); 
        Serial.print((char *)sendbuffer); 
        Serial.println("\"");
        // write the data
        BTLEserial.write(sendbuffer, sendbuffersize);
    }

    //*************************************************************SD Card

    File dataFile = SD.open("bg_log.txt", FILE_WRITE);
    
    // if the file is available, write to it:
    if (dataFile) {
        dataFile.println(scaledPressureString);
        dataFile.close();
        Serial.print("Writing -> ");
        Serial.println(scaledPressureString);
    }
    // if the file isn't open, pop up an error:
    else {
        Serial.println("error opening bg_log.txt");
    }
}//*******************************************************************end main loop
