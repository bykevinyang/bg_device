//
//        l1           l2              l3
//                              ________________ t3
// t1 ____________             /
//                \______ t2 _/
//       d1           d2              d3                            
//                 ___________
//    ____________/           \
//                             \________________

$fn = 200;


r1 = 10;
t1 = 1;

r2 = 12.5;
t2 = 3;

r3 = 7.5;
t3 = 2;

r4 = 10;
t4 = 2;

l1 = 25;
l2 = 10;
li = 15;
l3 = 15;
lo = 15;
l4 = 25;

module venturi(){
difference(){
    cylinder(l1, r1+t1, r2+t2);
    cylinder(l1, r1, r2);
}
translate([0,0,l1]){
    difference(){
        cylinder(l2, r2+t2, r2+t2);
        cylinder(l2, r2, r2);
    }
    translate([0,0,l2]){
        difference(){
            cylinder(li, r2+t2, r3+t3);
            cylinder(li, r2, r3);
        }
        translate([0,0,li]){
            difference(){
                cylinder(l3, r3+t3, r3+t3);
                cylinder(l3, r3, r3);
            }
            translate([0,0,l3]){
                difference(){
                    cylinder(lo, r3+t3, r4+t4);
                    cylinder(lo, r3, r4);
                }
                translate([0,0,lo]){
                    difference(){
                        cylinder(l4, r4+t4, r4+t4);
                        cylinder(l4, r4, r4);
                    }
                }
            }
        }
    }
}
}

difference(){
	union(){
		venturi();
		ramp();
	}

union(){
	translate([0,0,l1+l2/2]) rotate([0,90,0]) cylinder(30, 2, 2);
	translate([0,0,l1+l2+li+l3/2]) rotate([0,90,0]) cylinder(30, 2, 2);
}
}

t_ramp = 6;
module ramp(){
	intersection(){
		union(){
			translate([0,0,l1]){
				difference(){
					cylinder(l2,r2+t_ramp,r2+t_ramp);
					cylinder(l2,r2,r2);
				}
			}
			translate([0,0,l1+l2]){
				difference(){
					cylinder(li,r2+t_ramp,r3+t_ramp);
					cylinder(li,r2,r3);
				}
			}
			translate([0,0,l1+l2+li]){
				difference(){
					cylinder(l3,r3+t_ramp,r3+t_ramp);
					cylinder(l3,r3,r3);
				}
			}
			translate([0,0,l1+l2+li+l3]){
				difference(){
					cylinder(lo,r3+t_ramp,r4+t_ramp);
					cylinder(lo,r3,r4);
				}
			}
		}
	
		union(){
			translate([0,-2.5,l1]) cube([30,5,l2+li+l3]);
			intersection(){
				translate([0,0,l1+l2+li+l3]) cylinder(lo,r3+t_ramp,r3);
				translate([0,-2.5,l1]) cube([40,5,l2+li+l3+l4]);
			}
		}
	}
}

//// smoothness
//$fs=.1; // [.1:5]
//$fa=5;
//
//l1 = 20;
//d1 = 30;
//t1 = 2;
//
//l2 = 10;
//d2 = 20;
//t2 = 2;
//
//angle_in = 10;
//r_in = (d1/2 - d2/2)/sin(angle_in);
//l_in = r_in*cos(angle_in);
//
//l3 = 20;
//d3 = 40;
//t3 = 2;
//
//angle_out = 25;  // might have to be lower to reduce 
//r_out = (d3/2 - d2/2)/sin(angle_out);
//l_out = r_out*cos(angle_out);
//
//d_end = d1;
//angle_end = 5;
//r_end = (d_end/2 - d3/2)/sin(angle_end);
//l_end = r_end*cos(angle_end);
//t_end = t1;
//
//difference(){
//    cylinder(l1, d1/2 + t1/2, d1/2 + t1/2);
//    cylinder(l1, d1/2, d1/2);
//}
//
//
//translate([0, 0, l1]){
//    difference(){
//        cylinder(l_in, d1/2 + t1/2, d2/2 + t2/2);
//        cylinder(l_in, d1/2, d2/2);
//    }
//}
//
//
//translate([0, 0, l1 + l_in]){
//    difference(){
//        cylinder(l2, d2/2 + t2/2, d2/2 + t2/2);
//        cylinder(l1, d2/2, d2/2);
//    }
//}
//
//translate([0, 0, l1 + l_in + l2]){
//    difference(){
//        cylinder(l_out, d2/2 + t2/2, d3/2 + t3/2);
//        cylinder(l_out, d2/2, d3/2);
//    }
//}
//
//translate([0, 0, l1 + l_in + l2 + l_out]){
//    difference(){
//        cylinder(l3, d3/2 + t3/2, d3/2 + t3/2);
//        cylinder(l1, d3/2, d3/2);
//    }
//}
//
//!translate([0, 0, 0l1 + l_in + l2 + l_out + l3]){
//    difference(){
//        cylinder(l_end, d3/2 + t3/2, r_end + t_end/2);
//        cylinder(l_end, d3/2, r_end);
//    }
//}
//
